<?php

$bg_image = "background.png";

$MOBILE = false;
if(
	stripos($_SERVER['HTTP_USER_AGENT'], 'iPad') ||
	stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone') ||
	stripos($_SERVER['HTTP_USER_AGENT'], 'Android')
){
	$MOBILE = true;
}

function get_xkcd(){
	$cont = file_get_contents('https://xkcd.com/');
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*imgs.xkcd.com\/comics\/([a-z0-9_]+)\.png.*$/', '\1', $cont);
	return "https://imgs.xkcd.com/comics/{$data}.png";
}

/* https://www.avclub.com/dilbert-scott-adams-dropped-from-papers-race-comments-1850159479
function get_dilbert(){
	$cont = file_get_contents('http://dilbert.com');
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*?assets.amuniversal.com\/([a-z0-9]+).*$/', '\1', $cont);
	return "http://assets.amuniversal.com/{$data}";
}
*/

function get_phd(){
	$cont = file_get_contents('http://phdcomics.com');
	$cont = str_replace("\n", '', $cont);
	$regex = '/^.*?name=comic[2]? src=http:\/\/www.phdcomics.com\/comics\/archive\/([^" >]+\.(jpg|gif))[" >].*$/';
	$data = preg_replace($regex, '\1', $cont);
	if($data == $cont){
		$comicid = preg_replace(
			'/^.*?archive.php\?comicid=([0-9]+)><img height=[0-9]+ width=[0-9]+ src=[^ ]+prev_button.gif .*$/', '\1', $cont
		);
		$cont = file_get_contents("http://phdcomics.com/comics/archive.php?comicid={$comicid}");
		$cont = str_replace("\n", '', $cont);
		$data = preg_replace($regex, '\1', $cont);
		if($data == $cont){
			$data == null;
		}
	}
	if($data == null){
		return "";
	} else {
		return "http://www.phdcomics.com/comics/archive/{$data}";
	}
}

function get_commit(){
	$lcont = file_get_contents('https://www.commitstrip.com/en/');
	$lcont = str_replace("\n", '', $lcont);
	$cont_url = preg_replace('/^.*?<meta property=\"og:url\" content=\"(http[^\"]+)\".*$/', '\1', $lcont);

	$cont = file_get_contents($cont_url);
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*?<p><img class=\"align.*? src=\"([^\"]+)\".*?$/', '\1', $cont);

	return $data;
}

function get_foxtrot(){
	$lcont = file_get_contents('http://www.foxtrot.com/feed/');
	$lcont = str_replace("\n", '', $lcont);
	$cont_url = preg_replace('/^.*?<link>.*?foxtrot.com\/([0-9]+\/[0-9]+\/[0-9]+\/[a-z\-]+\/).*$/', '\1', $lcont);
	$cont_url = "http://www.foxtrot.com/{$cont_url}";

	$stream_opts = array(
		'http' => array(
			'header' => 'Cookie: smarter-navigation[query]=%7B%22feed%22%3A%22feed%22%7D; smarter-navigation[url]=https%3A%2F%2Fwww.foxtrot.com%2Ffeed%2F'
		)
	);
	$context = stream_context_create($stream_opts);

	$cont = file_get_contents($cont_url, FALSE, $context);

	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*?https:\/\/foxtrot.com\/wp-content\/uploads\/([0-9]+\/[0-9]+\/[^\.]+\.(?:png|jpg)).*$/', '\1', $cont);
	return "http://www.foxtrot.com/wp-content/uploads/{$data}";
}

function get_dino(){
	$cont = file_get_contents('http://www.qwantz.com/index.php');
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*?content="https:\/\/www.qwantz.com\/comics\/([^\.]+)\.png.*$/', '\1', $cont);
	return "https://www.qwantz.com/comics/{$data}.png";
}

function get_explosm(){
	$cont = file_get_contents('https://explosm.net/comics/latest');
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*static.explosm.net\/([^\.]+\.(gif|png)).*$/', '\1', $cont);
	return "https://static.explosm.net/{$data}";
}

function get_smbc(){
	$cont = file_get_contents('https://www.smbc-comics.com/index.php');
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*src="([^"]+)" id="cc-comic".*$/', '\1', $cont);
	return $data;
}

function get_onion(){
	$cont = file_get_contents('https://www.theonion.com/c/editorial-cartoon');
	$cont = str_replace("\n", '', $cont);
	$cont_url = preg_replace('/^.*?"@type":"ListItem","position":1,"url":"([^\"]+)".*$/', '\1', $cont);
	$cont = file_get_contents($cont_url);
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*?(https:\/\/[^ ]+) 965w.*$/', '\1', $cont);
	return $data;
}

function get_efc(){
	$cont = file_get_contents('http://extrafabulouscomics.com/');
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*<wow-image.*?data-pin-media="([^"]+)".*$/', '\1', $cont);
	return $data;
}
/*function get_efc(){
	$cont = file_get_contents('http://extrafabulouscomics.com/');
	$cont = str_replace("\n", '', $cont);
	$cont_url = preg_replace(
		'/^.*?a href="https:\/\/www.extrafabulouscomics.com\/__([0-9]+)".*$/', '\1', $cont
	);

	$cont = file_get_contents("https://www.extrafabulouscomics.com/__${cont_url}");
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*? data-pin-url="https:\/\/www.extrafabulouscomics.com\/__'.$cont_url.'".*? src="([^\"]+)".*$/', '\1', $cont);

	return $data;
}*/

function get_pbf(){
	$cont = file_get_contents('http://pbfcomics.com/');
	$cont = str_replace("\n", '', $cont);
	$data = preg_replace('/^.*<noscript><img src=\'([^\']+)\'.*$/', '\1', $cont);
	return $data;
}

$xkcd = get_xkcd();
#$dilbert = get_dilbert();
$phd = get_phd();
$commit = get_commit();
$foxtrot = get_foxtrot();
$dino = get_dino();
$explosm = get_explosm();
$smbc = get_smbc();
$onion = get_onion();
$efc = get_efc();
$pbf = get_pbf();

?>
<html>

<head>
<?php if($MOBILE){ ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="HandheldFriendly" content="true" />
<?php } ?>
<style>
<!--

img {
	padding: 10px 0px;
	max-width: 1024px;
<?php if($MOBILE){ ?>
	width: 100%;
<?php } ?>
}

body {
	background-image: url("<?php echo $bg_image; ?>");
}

//-->
</style>
</head>

<body>

<center>
<a href="https://xkcd.com"><img src="<?php echo $xkcd; ?>" /></a>
<!--<br />
<a href="http://dilbert.com"><img src="<?php echo $dilbert; ?>" /></a>-->
<br />
<a href="http://phdcomics.com"><img src="<?php echo $phd; ?>" /></a>
<br />
<a href="https://www.commitstrip.com/"><img src="<?php echo $commit; ?>" /></a>
<br />
<a href="http://www.foxtrot.com"><img src="<?php echo $foxtrot; ?>" /></a>
<br />
<a href="http://www.qwantz.com"><img src="<?php echo $dino; ?>" /></a>
<br />
<a href="http://explosm.net"><img src="<?php echo $explosm; ?>" /></a>
<br />
<a href="https://www.smbc-comics.com"><img src="<?php echo $smbc; ?>" /></a>
<br />
<a href="https://www.theonion.com/c/editorial-cartoon"><img src="<?php echo $onion; ?>" /></a>
<br />
<a href="http://extrafabulouscomics.com"><img src="<?php echo $efc; ?>" /></a>
<br />
<a href="http://pbfcomics.com"><img src="<?php echo $pbf; ?>" /></a>
</center>

</body>
</html>
