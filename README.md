simple_comics 1.0.1
===================

Very simple comics strip scraper page for the current xkcd, Dilbert, Piled Higher and Deeper, Foxtrot, Dinosaur Comics, Cyanide & Happiness, Saturday Morning Breakfast Cereal, Extra Fabulous Comics, and Perry Bible Fellowship.
